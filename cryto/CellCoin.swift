//
//  CellCoin.swift
//  cryto
//
//  Created by VN Grand M on 3/21/22.
//

import UIKit

class CellCoin: UITableViewCell {
    static let identifier="coincell"
    static func nib()->UINib{
        return UINib(nibName: "coincell", bundle: nil)
    }
    public func config(image:UIImage, id:String,name:String,price:String,prichange:String,
                           marketchange:String, prichangepercent:String){
            img.image=image
            self.id.text=id
            self.name.text=name
            self.price.text=price
            self.pricechange.text=prichange
            self.prichangepercent.text=prichangepercent
            self.marketchange.text=marketchange
    }
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var pricechange: UILabel!
    @IBOutlet weak var marketchange: UILabel!
    @IBOutlet weak var prichangepercent: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}
