//
//  commonServices.swift
//  cryto
//
//  Created by VN Grand M on 4/20/22.
//

import Foundation
import Firebase
import UIKit
class CommonServices {
    static func authfirebase() -> String {
        let usser =  Auth.auth().currentUser
        var currentus :String
        if (usser?.uid) != nil{
            currentus = usser!.uid
        }else{
            currentus = UserDefaults.standard.dictionaryWithValues(forKeys: ["id"])["id"] as! String
        }
        return currentus
    }
    static func loadImgFromUrl(url : String) -> Data{
            let readurl =  URL(string: url)
            if let readurl =  readurl {
                if let data = try?  Data(contentsOf: readurl){
                    return data
                }
            }
        return Data()
    }
    static func dbFireBaseInit(){
        if(FirebaseApp.app() == nil){
            FirebaseApp.configure()
        }
    }
    static func loaddingScreen() -> UIAlertController{
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        alert.view.tintColor = UIColor.black
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50)) as UIActivityIndicatorView
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        alert.view.addSubview(loadingIndicator)
        return alert
  
    }
}
