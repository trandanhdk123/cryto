//
//  Allcoin.swift
//  cryto
//
//  Created by VN Grand M on 3/31/22.
//

import UIKit
protocol favouritedelegate {
    func favourite(coinid :String, type : String, childkey : String)
    
    
}
class Allcoin: UITableViewCell {

    static let identifier = "Allcoin"
    static func nib()->UINib{
        return UINib(nibName: "Allcoin", bundle: nil)
    }
    public func config(image:UIImage, id:String,name:String,price:String){
            self.coinimg.image=image
            self.coinname.text=id
            self.smcoinname.text=name
            self.priceone.text=price

    }

    @IBOutlet weak var coinimg: UIImageView!
    
    @IBOutlet weak var coinname: UILabel!
 
    @IBOutlet weak var smcoinname: UILabel!
    
    @IBOutlet weak var priceone: UILabel!

    @IBOutlet weak var btnfavou: UIButton!

    public var childkey = ""
    public var favouriteflag = 0
    var delegate : favouritedelegate?
    @IBAction func clickfavou(_ sender: Any) {
        if(favouriteflag == 0) {
            self.btnfavou.setImage(UIImage(systemName: "heart.fill"), for: .normal)
          
            self.delegate!.favourite( coinid: self.coinname.text!, type: "favourite" ,  childkey: childkey  )
        }else{
            self.btnfavou.setImage(UIImage(systemName: "heart"), for: .normal)
            self.delegate!.favourite(coinid: self.coinname.text!, type: "unfavourite" , childkey: childkey  )
        }
        self.btnfavou.isEnabled = false

    
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
