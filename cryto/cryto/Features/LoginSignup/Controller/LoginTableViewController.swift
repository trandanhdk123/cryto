//
//  LoginTableViewController.swift
//  cryto
//
//  Created by VN Grand M on 4/14/22.
//

import UIKit
import SwiftHEXColors
import Firebase
protocol loginProtocol {
    func loginWithFireBase()
}
class LoginTableViewController: UITableViewController, UIGestureRecognizerDelegate, UITextFieldDelegate, loginProtocol {
    @IBOutlet var tableViewLogin: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackBtnIconNav(imgName: "signupBackIcon")
        hideBackBtn()
        if (UserDefaults.standard.bool(forKey: "Loggedin")  ){
            changeRootToTabView()
        }
 

    }
    
    override func viewWillAppear(_ animated: Bool) {
        isInVisibleNavBar(isVisible: true, animated: true)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        isInVisibleNavBar(isVisible: false, animated: true)
       
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 8
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return 1
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
         headerView.backgroundColor = UIColor.init(hexString: "#FFFFFF")
        return headerView
 
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 64
        case 1:
            return 45
        case 2:
            return 27
        case 3:
            return 21
        case 4:
            return 45
        case 5:
            return 36
        case 6:
            return 16
        default:
            return 26
        }
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 125
        case 1,2:
            return 75
        case 3,5,7:
            return 17
        case 4:
            return 80
        case 6:
            return 44
        default:
            return 10
        }
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell  = tableView.dequeueReusableCell(withIdentifier: "bannerSection", for: indexPath) as! CellLogin
      
            return cell
        case 1,2:
            let cell  = tableView.dequeueReusableCell(withIdentifier: "userInputSection", for: indexPath) as! CellLogin
            if(indexPath.section == 1){
                cell.lblTextfieldTitle.text  = "EMAIL"
                cell.textfield.placeholder = "Hãy nhập Email của bạn"
                cell.textfield.accessibilityIdentifier = "email"
                cell.textfield.delegate = self
                cell.textfield.keyboardType = .emailAddress
            }else{
                cell.lblTextfieldTitle.text  = "MẬT KHẨU"
                cell.textfield.placeholder = "Hãy nhập mật khẩu của bạn"
                cell.textfield.accessibilityIdentifier = "password"
                cell.textfield.delegate = self
                cell.textfield.isSecureTextEntry = true
            }
            return cell
        case 3:
            let cell  = tableView.dequeueReusableCell(withIdentifier: "forgotPassWordSection", for: indexPath) as! CellLogin
            cell.lblForgotPassword.setTitle("QUÊN MẬT KHẨU", for: .normal)
            return cell
        case 4:
            let cell  = tableView.dequeueReusableCell(withIdentifier: "loginBtnSection", for: indexPath) as! CellLogin
            self.shadowBtn(btn: cell.loginBtn)
            self.borderRadius(btn: cell.loginBtn)
            self.btnBackgroundColor(btn: cell.loginBtn, color: "#0062CF")
            cell.delegate = self
            return cell
        case 5:
            let cell  = tableView.dequeueReusableCell(withIdentifier: "lblSignInWithOtherMethod", for: indexPath) as! CellLogin
            cell.lblLoginOtherMethod.text = "— Đăng nhập với —"
            return cell
        case 6:
            let cell  = tableView.dequeueReusableCell(withIdentifier: "socialLoginSection", for: indexPath) as! CellLogin
            return cell
        case 7:
            let cell  = tableView.dequeueReusableCell(withIdentifier: "moveToSignUpSection", for: indexPath) as! CellLogin
            return cell
        default:
            return CellLogin()
        }
    }
    var userInputEmailPassWord = [String:String]()
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if let textfieldID = textField.accessibilityIdentifier {
        userInputEmailPassWord[textfieldID] = textField.text
        }
    }
    func loginWithFireBase() {
        CommonServices.dbFireBaseInit()
        if let email = userInputEmailPassWord["email"],
            let password = userInputEmailPassWord["password"]{
            networkingLogin(email: email, password: password)
        }
    }
    func networkingLogin(email:String, password:String){
        AuthorNetworking.loginWithUserNEmail(email: email, password: password){(isLoggedIn) in
            if(isLoggedIn){
                UserDefaults.standard.set(true, forKey: "Loggedin")
                UserDefaults.standard.set(Auth.auth().currentUser!.email, forKey: "email")
                UserDefaults.standard.set(Auth.auth().currentUser!.uid, forKey: "id")
                self.changeRootToTabView()
            }else{
                print("error")
            }
  
        }
    }
}
