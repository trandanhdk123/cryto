//
//  SignUpTableViewController.swift
//  cryto
//
//  Created by VN Grand M on 4/14/22.
//

import UIKit
import Firebase
protocol signupProtocol {
    func signup()
}


class SignUpTableViewController: UITableViewController, UITextFieldDelegate, signupProtocol {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = UIColor.init(hexString: "#0062CF")

    }
    var userData = [String: String]()
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell  = tableView.dequeueReusableCell(withIdentifier: "avatarSection", for: indexPath) as! CellSignUp
            cell.selectionStyle = .none
            cell.isUserInteractionEnabled =  false
            cell.viewAvatar.backgroundColor = UIColor.init(hexString: "#0062CF")
       
            return cell
        case 1,2,3,4,5:
            let cell  = tableView.dequeueReusableCell(withIdentifier: "inputFieldSection", for: indexPath) as! CellSignUp
            cell.selectionStyle = .none
            
            switch indexPath.section {
            case 1:
                cell.lblTitle.text = "TÊN"
                cell.textfield.placeholder =  "Nhập tên của bạn"
                cell.textfield.accessibilityIdentifier = "name"
                cell.textfield.delegate = self
            case 2:
                cell.lblTitle.text = "HỌ"
                cell.textfield.placeholder =  "Nhập họ"
                cell.textfield.accessibilityIdentifier = "Firstname"
                cell.textfield.delegate = self
            case 3:
                cell.lblTitle.text = "EMAIL"
                cell.textfield.placeholder =  "Nhập email của bạn"
                cell.textfield.accessibilityIdentifier = "email"
                cell.textfield.delegate = self
                cell.textfield.keyboardType = .emailAddress
            case 4:
                cell.lblTitle.text = "MẬT KHẨU"
                cell.textfield.placeholder =  "Nhập mật khẩu"
                cell.textfield.accessibilityIdentifier = "password"
                cell.textfield.isSecureTextEntry =  true
                cell.textfield.delegate = self
            case 5:
                cell.lblTitle.text = "XÁC NHẬN MẬT KHẨU"
                cell.textfield.placeholder =  "Nhập lại mật khẩu"
                cell.textfield.isSecureTextEntry =  true
                cell.textfield.delegate = self
            default:
                cell.lblTitle.text = "XÁC NHẬN MẬT KHẨU"
            }
            return cell
        case 6:
            let cell  = tableView.dequeueReusableCell(withIdentifier: "btnSection", for: indexPath) as! CellSignUp
            cell.selectionStyle = .none
            self.shadowBtn(btn: cell.btnSignup)
            self.borderRadius(btn: cell.btnSignup)
            self.btnBackgroundColor(btn: cell.btnSignup, color: "#0062CF")
            cell.delegate = self
            return cell
        default:
            return CellSignUp()
        }
    }
    func signup(){
        CommonServices.dbFireBaseInit()
        if let email = userData["email"] , let password = userData["password"] {
            signupWithEmailNPassword(email: email, password: password)
        }
    }
    func signupWithEmailNPassword(email:String, password:String){
        AuthorNetworking.signupWithEmailNPassword(email: email, password: password ){(isCreatedUser) in
            if isCreatedUser {
                
                self.saveUserToDB()
                self.navigationController?.setNavigationBarHidden(true, animated: true)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    func saveUserToDB(){
        AuthorNetworking.saveUserDataToDB(userData: self.userData){_ in
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField.placeholder != "Nhập lại mật khẩu" ){
            userData[textField.accessibilityIdentifier!] = textField.text
        }
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           let headerView = UIView()
            headerView.backgroundColor = UIColor.init(hexString: "#FFFFFF")
           return headerView
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 0
        case 1:
            return 46
        case 2,3,4,5:
            return 43
        default:
            return 76
        }
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 105
        case 1,2,3,4,5:
            return 61
        case 6:
           return 80
        default:
            return 0
        }
    }
}
