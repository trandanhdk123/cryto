//
//  authorNetworking.swift
//  cryto
//
//  Created by VN Grand M on 4/21/22.
//

import Foundation
import UIKit
class AuthorNetworking{
    static func loginWithUserNEmail(email:String, password:String,completion : @escaping(Bool)->Void){
        NetworkServices.shared.firebaseAuthorWithEmailPassword(email: email, password: password){(isLoginSuccess) in
            if(isLoginSuccess){
                NetworkServices.shared.checkIfEmailIsVerified(){(emailIsVerified) in
                    completion(emailIsVerified)
                }
            }else{
                completion(false)
            }
        }
    }
    static func signupWithEmailNPassword(email:String, password:String,completion: @escaping(Bool)->Void){
        NetworkServices.shared.firebaseCreateUserWithEmailPassword(email: email, password: password){(isSuccess) in
            if(isSuccess){
                NetworkServices.shared.firebaseIsSendEmailVerified(){(isSendEmail) in
                    completion(isSendEmail)
                }
            }else{
                completion(false)
            }
        }
    }
    static func saveUserDataToDB(userData:[String:String], completion:@escaping(Bool)->Void){
        NetworkServices.shared.saveUserDataToDB(userData: userData){
            completion(true)
        }
    }

}
