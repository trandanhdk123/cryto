//
//  cellLogin.swift
//  cryto
//
//  Created by VN Grand M on 4/21/22.
//

import Foundation
import UIKit
class CellLogin  : UITableViewCell {

    @IBOutlet weak var banner: UIImageView!
    @IBOutlet weak var lblTextfieldTitle: UILabel!
    @IBOutlet weak var textfield: UITextField!
    @IBOutlet weak var lblForgotPassword: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var appleLogin: UIImageView!
    @IBOutlet weak var facebookLogin: UIImageView!
    @IBOutlet weak var googleLogin: UIImageView!
    @IBOutlet weak var lblLoginOtherMethod: UILabel!
    @IBOutlet weak var moveToSignUpBtn: UIButton!
    override func awakeFromNib() {

    }

    var delegate: loginProtocol?
    @IBAction func loginBtnAction(_ sender: Any) {
        delegate?.loginWithFireBase()
        
    }

}
