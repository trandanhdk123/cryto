//
//  cellSignUp.swift
//  cryto
//
//  Created by VN Grand M on 4/21/22.
//

import Foundation
import  UIKit
class CellSignUp : UITableViewCell {
    @IBOutlet weak var avatar: UIView!
    @IBOutlet weak var viewAvatar: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var textfield: UITextField!
    @IBOutlet weak var btnSignup: UIButton!
    override func awakeFromNib() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
               self.addGestureRecognizer(tap)
    }
    @objc func handleTap() {
        textfield.resignFirstResponder()

    }
    var delegate : signupProtocol?

    @IBAction func signupBtn(_ sender: Any) {
        print("a")
            self.delegate?.signup()
           
        
    }
}
