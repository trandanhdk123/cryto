//
//  CoinDetail.swift
//  cryto
//
//  Created by VN Grand M on 3/21/22.
//

import UIKit

class CoinDetailVC: UIViewController,delegatetest {
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var container: UIView!
    @IBAction func segmentchange(_ sender: Any) {
        if segment.selectedSegmentIndex == 0 {
                
                    add(asChildViewController: firstViewController)
        
                } else {
                    remove(asChildViewController: firstViewController)
                    
                }
    }
    public var id:String?
    func passdata(data: String) {
        self.id = data
    }
    var delegate :delegatetest?
    override func viewDidLoad() {
        super.viewDidLoad()
       

        add(asChildViewController: firstViewController)
        self.segment.removeAllSegments()
        self.segment.insertSegment(withTitle: "Chart", at: 0, animated: false)
        self.segment.insertSegment(withTitle: "Trade", at: 1, animated: false)
        self.segment.insertSegment(withTitle: "Holding", at: 2, animated: false)
        self.segment.selectedSegmentIndex=0
    }
    private func add(asChildViewController viewController: UIViewController) {
        addChild(viewController)
         container.addSubview(viewController.view)
        
        viewController.didMove(toParent: self)
     }
     private func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
     }
    private lazy var firstViewController: DetailViewController = {

        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "seg1") as! DetailViewController
        self.delegate = viewController
        self.delegate?.passdata(data: self.id!)
//        print(self.delegate?.checkdatasrc!(data: self.id!) as Any)
        viewController.view.frame = CGRect(x: 0, y: 0, width: container.frame.size.width, height: container.frame.size.height);
        viewController.view.isUserInteractionEnabled = true;
        self.add(asChildViewController: viewController)
        return viewController
    }()

//    private lazy var secondViewController:  MarketViewController = {
//        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//        var viewController = storyboard.instantiateViewController(withIdentifier: "seg2") as! MarketViewController
//        self.delegate = viewController
//        self.delegate?.passdata(data: self.id!)
//        self.add(asChildViewController: viewController)
//
//        return viewController
//    }()

    

}
