//
//  DetailViewController.swift
//  cryto
//
//  Created by VN Grand M on 3/22/22.
//

import UIKit
import Charts
import Alamofire
import Moya

public class DateValueFormatter: NSObject, AxisValueFormatter {
    private let dateFormatter = DateFormatter()
    
    override init() {
        super.init()
        dateFormatter.dateFormat = "dd MMM"
    }
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return dateFormatter.string(from: Date(timeIntervalSince1970: value))
    }
}
class DetailViewController: UIViewController, UITableViewDelegate,UITableViewDataSource,delegatetest, changesegment{
    func changesegment() {
        tableviewDetail.reloadData()
        print("change")
    }
    
    @IBOutlet weak var tableviewDetail: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("a")
        tableviewDetail.register(UINib(nibName: "ChartSectionCoinDetailTableViewCell", bundle: nil), forCellReuseIdentifier: ChartSectionCoinDetailTableViewCell.identifier)
        tableviewDetail.register(ListPriceSectionCell.nib(), forCellReuseIdentifier: ListPriceSectionCell.identifier)
        tableviewDetail.delegate=self
        tableviewDetail.dataSource=self

    }
    public var id:String = ""
    func passdata(data: String) {
        self.id = data
        print("A")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height:CGFloat = CGFloat()
        if indexPath.row == 0 {
            height = 500
        }else{
            height = 50
        }
        return height
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row  > 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ListPriceSectionCell") as! ListPriceSectionCell
            loadDataForCell(id: self.id,cell: cell, row : indexPath.row)
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChartSectionCoinDetailTableViewCell") as! ChartSectionCoinDetailTableViewCell
        cell.id = self.id
        cell.delegate = self
        loadLineChartWithTime(time: cell.charttime, cell: cell)
        loadDataForCell(id: self.id, cell: cell, row : indexPath.row)
        return cell
    }
    func loadLineChartWithTime(time:String, cell :  ChartSectionCoinDetailTableViewCell) {
        var dataEntry = [ChartDataEntry]()
        DetailCoinNetworking.updateDataToChartDaTaEntry(time: time, id: self.id, dataEnTry: dataEntry){(dataEntry) in
            DetailCoinNetworking.updateDataSetTochart(dataEntry: dataEntry){(dataLineChart) in
                cell.linechartview.data = dataLineChart
            }
        }
    }
    func  loadDataForCell(id:String, cell : Any, row : Int) {

        DetailCoinNetworking.getCoinData(id: id){(coinData) in
            switch row {
            case 0 :
                self.loadDataForChartCell(coinData: coinData, cell: cell as! ChartSectionCoinDetailTableViewCell)
            default :
                    self.loadDataForAnotherCell(coinData: coinData, cell: cell as! ListPriceSectionCell)
            }
        }
    }
    func loadDataForChartCell(coinData:CoinDetail,cell: ChartSectionCoinDetailTableViewCell){
        let imgurl = coinData.image!["large"] as! String
        let data =  CommonServices.loadImgFromUrl(url: imgurl)
        let cell = cell
        cell.img.image = UIImage(data: data)
        cell.name.text = coinData.symbol?.uppercased()
        let current_price = coinData.market_data!["current_price"] as! [String:Double]
        cell.price.text = String(current_price["usd"]!)
        cell.price.text?.append("$")
        cell.price.textColor = UIColor.green
    }
    func loadDataForAnotherCell(coinData:CoinDetail,cell:ListPriceSectionCell){
        cell.labelprice.text = String( coinData.market_data!["price_change_24h"] as! Double)
        cell.labeltext.text = "Price Change 24h"
    }

}
