//
//  coinWebsocketData.swift
//  cryto
//
//  Created by VN Grand M on 4/21/22.
//

import Foundation
class CoinChartData : Decodable{
    
    let prices: [[Double]]
    let market_caps : [[Double]]
    let total_volumes : [[Double]]
    init(prices: [[Double]], market_caps:[[Double]], total_volumes: [[Double]] )  {
        self.prices = prices
        self.market_caps = market_caps
        self.total_volumes = total_volumes
        }
}
