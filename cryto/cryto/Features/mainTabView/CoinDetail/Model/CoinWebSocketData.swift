//
//  CoinWebSocketData.swift
//  cryto
//
//  Created by VN Grand M on 4/21/22.
//

import Foundation
import ObjectMapper
import UIKit
class CoinWebSocketData :Mappable{
    public var e: String?     // Event type
    public var E: Double?   // Event time
    public var s: String?
    public var k: k?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.e <- map["e"]
        self.E <- map["E"]
        self.s <- map["s"]
        self.k <- map["k"]
    }

    
    
}
class k : Mappable{
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.t <- map["t"]
        self.T <- map["T"]
        self.s <- map["s"]
        self.i <- map["i"]
        self.f <- map["f"]
        self.L <- map["L"]
        self.o <- map["o"]
        self.c <- map["c"]
        self.h <- map["h"]
        self.l <- map["l"]
        self.v <- map["v"]
        self.n <- map["n"]
        self.x <- map["x"]
        self.q <- map["q"]
        self.V <- map["V"]
        self.Q <- map["Q"]
        self.B <- map["B"]
    }
    
    
    public var t: Double? // Kline start time
    public var T: Double?
    public var s: String? // Symbol
    public var i: String?     // Interval
    public var f: Double?     // First trade ID
    public var L: Double?     // Last trade ID
    public var o: Double?// Open price
    public var c: Double?
    public var h: Double?  // High price
    public var l: Double?  // Low price
    public var v: String?    // Base asset volume
    public var n: String?       // Number of trades
    public var x: Bool?     // Is this kline closed?
    public var q: String?  // Quote asset volume
    public var V: String?    // Taker buy base asset volume
    public var Q: String?   // Taker buy quote asset volume
    public var B: String?
}
