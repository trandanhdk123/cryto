//
//  DetailCoinNetworking.swift
//  cryto
//
//  Created by VN Grand M on 4/21/22.
//

import Foundation
import Charts
class DetailCoinNetworking{
    static func updateDataToChartDaTaEntry(time:String,id:String,dataEnTry: [ChartDataEntry], completion : @escaping([ChartDataEntry])->Void){
        var dataEnTry = dataEnTry
        NetworkServices.shared.getChartDataFromAPI(time: time, id:id ){(dataFromAPI) in
            for i in 1..<dataFromAPI.prices.count{
                let xvalue = dataFromAPI.prices[i][0]/1000
                let yvalue = dataFromAPI.prices[i][1]
                dataEnTry.append(ChartDataEntry(x: xvalue,y:yvalue))
            }
            completion(dataEnTry)
        }
    }
    static func updateDataSetTochart(dataEntry:[ChartDataEntry],completion: @escaping(LineChartData)->Void){
        let dataSet =  LineChartDataSet(entries: dataEntry, label: "")
        dataSet.label = ""
        dataSet.drawCirclesEnabled = false
        let data = LineChartData(dataSet: dataSet)
        completion(data)
    }
    static func getCoinData(id:String,completion:@escaping(CoinDetail)->Void){
        NetworkServices.shared.getDataOfCoinByCoinID(id: id){(coinData)in
                completion(coinData)

        }
    }
    
    
}
