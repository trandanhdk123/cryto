//
//  chartSectionCoinDetailTableViewCell.swift
//  cryto
//
//  Created by VN Grand M on 4/13/22.
//

import UIKit
import Charts
import Moya
protocol changesegment {
    func changesegment()
}
class ChartSectionCoinDetailTableViewCell: UITableViewCell,URLSessionWebSocketDelegate{
    @IBOutlet weak var linechartview:LineChartView!
    @IBOutlet weak var segmentchangetime: UISegmentedControl!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var coinPriceUsd: UIButton!
    @IBOutlet weak var symbol: UIButton!
    
    
    static let identifier = "ChartSectionCoinDetailTableViewCell"
    static func nib()->UINib{
        return UINib(nibName: "ChartSectionCoinDetailTableViewCell", bundle: nil)
    }
    var delegate : changesegment?
    var charttime =  "1"
    var id : String?
    var websocket:URLSessionWebSocketTask?
    var oldprice : String = ""
    var jsonArray: [String:AnyObject] = [:]
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        chartSetup()
        self.isUserInteractionEnabled = true
        segmentSetUp()
        webSocketSetUp()
        borderRadius(btn: coinPriceUsd)
        
    }
    
    
    
    
    
    func webSocketSetUp(){
        let session = URLSession(configuration: .default,delegate: self,delegateQueue: OperationQueue())
     websocket = session.webSocketTask(with: URL(string: "wss://stream.binance.com:9443/ws/bnbbtc@kline_1m")!)
        websocket!.resume()
    }
    func chartSetup(){
        linechartview.rightAxis.enabled = false
        linechartview.legend.enabled = false
        let xAxis = linechartview.xAxis
        xAxis.drawAxisLineEnabled = false
        xAxis.labelPosition = .bottom
        xAxis.drawGridLinesEnabled = false
        xAxis.centerAxisLabelsEnabled = false
        xAxis.valueFormatter = DateValueFormatter()
    }
    func segmentSetUp(){
        self.segmentchangetime.removeAllSegments()
        self.segmentchangetime.insertSegment(withTitle: "1D", at: 0, animated: false)
        self.segmentchangetime.insertSegment(withTitle: "1M", at: 1, animated: false)
        self.segmentchangetime.insertSegment(withTitle: "1Y", at: 2, animated: false)
        self.segmentchangetime.selectedSegmentIndex=0
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    public func config(id:String, chartdata : LineChartData, price: String,  img:UIImage, name: String){
            self.id = id
            self.linechartview.data = chartdata
        self.name.text = name
        self.img.image = img
     

    }
        func ping(){
            websocket?.sendPing { error in
                if let error = error {
                    print(error)
                }
    
            }
        }
        func close(){
            websocket?.cancel(with: .goingAway, reason: "End".data(using : .utf8))
        }
        func send(){
            DispatchQueue.global().asyncAfter(deadline: .now()+100) {
                self.send()
                self.websocket?.send(.string("get data"), completionHandler: {
                    error in
                    if let error = error{
                        print("Error1 \(error)")
                    }
                })
            }
    
        }

    func loadDataToOutlet(dataGetFromWebSocket :  CoinWebSocketData){
        DispatchQueue.main.async {
           
            if let price =  dataGetFromWebSocket.k?.q!{
            
                self.coinPriceUsd.setTitle(String(format: "%.2f", Double(price)!*10), for: .normal)
                
                if(self.oldprice < price) {
                    self.symbol.setImage(UIImage(systemName: "arrowtriangle.up"), for: .normal)
                    self.symbol.backgroundColor = .green
                    self.coinPriceUsd.backgroundColor = .green
                }else{
                    self.symbol.setImage(UIImage(systemName: "arrowtriangle.down"), for: .normal)
                    self.symbol.backgroundColor = .red
                    self.coinPriceUsd.backgroundColor = .red
                }
                self.oldprice = price
            }
        }
    }
    @IBAction func changesegment(_ sender: Any) {
        if segmentchangetime.selectedSegmentIndex == 0 {
            self.segmentchangetime.selectedSegmentIndex=0
            charttime = "1"
        } else if segmentchangetime.selectedSegmentIndex == 1 {
            self.segmentchangetime.selectedSegmentIndex=1
            charttime = "30"
        
        }else{
            self.segmentchangetime.selectedSegmentIndex=2
            charttime = "365"
        }
        delegate?.changesegment()
    }
    func checkdatasrc(data: String) -> String {
        return "This is response ajax \(data)"
    }
    func receiveData(dataGetFromApi : Data){
        do {
            self.jsonArray = try ((JSONSerialization.jsonObject(with: dataGetFromApi, options: JSONSerialization.ReadingOptions()) as? [String:AnyObject])!)
        } catch {
          print(error)
        }
        if let data = self.jsonArray as? [String: AnyObject] {

          if let datamap = CoinWebSocketData(JSON: data)
            {
            self.loadDataToOutlet(dataGetFromWebSocket: datamap)
          
          }
        }
    }
    func receiveString (dataGetFromApi : String){
        if (dataGetFromApi.count != 0) {
        do{
            self.jsonArray = try (JSONSerialization.jsonObject(with: dataGetFromApi.data(using: String.Encoding.utf8)!, options: .allowFragments) as? [String: AnyObject])!
        }catch{
            print(error)
        }
        if let datamap = CoinWebSocketData(JSON: self.jsonArray)
            {
            self.loadDataToOutlet(dataGetFromWebSocket: datamap)
          }
        }
    }
        func receive(){
            websocket?.receive(completionHandler: { [weak self] result in
                switch result {
                case .success  (let message) :
                    switch message {
                    case .data(let data):
                        self!.receiveData(dataGetFromApi: data)

                        print("this is \(data)")
                    case .string(let message):
                        self!.receiveString(dataGetFromApi: message)
    
                        self!.receive()
                    @unknown default:
                        break
                    }
                case .failure (let error ):
                    print("this is error \(error)")
                    self!.close()
                    break
                    
    
                }
                
            })
    
    
        }
        func urlSession(_ session: URLSession, webSocketTask: URLSessionWebSocketTask, didOpenWithProtocol protocol: String?) {
            print("connect")
            ping()
            receive()
            send()
        }
        func urlSession(_ session: URLSession, webSocketTask: URLSessionWebSocketTask, didCloseWith closeCode: URLSessionWebSocketTask.CloseCode, reason: Data?) {
            print("close")
            close()
        }
}
