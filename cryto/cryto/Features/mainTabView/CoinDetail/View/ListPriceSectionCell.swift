//
//  listPriceSectionCell.swift
//  cryto
//
//  Created by VN Grand M on 4/13/22.
//

import UIKit

class ListPriceSectionCell: UITableViewCell {
    static let identifier = "ListPriceSectionCell"
    static func nib()->UINib{
        return UINib(nibName: "ListPriceSectionCell", bundle: nil)
    }
    @IBOutlet weak var labeltext: UILabel!
    @IBOutlet weak var labelprice: UILabel!
    public func config(labeltext:String, labelprice :  String ){
        self.labeltext.text = labeltext
        self.labelprice.text = labelprice
     

    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
