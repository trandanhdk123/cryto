//
//  dashboardTableVC.swift
//  cryto
//
//  Created by VN Grand M on 4/20/22.
//
import UIKit
extension DashboardViewController : UITableViewDataSource, UITableViewDelegate,favouritedelegate{
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(10)
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.init(hexString: "#FFFFFF")
        return view
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let dashview = storyboard.instantiateViewController(identifier: "detail") as! CoinDetailVC
        self.delegate = dashview
        self.delegate?.passdata(data: listCoinDataSrc[indexPath.row].id!)
        dashview.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(dashview, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
     func tableView(_ tableView: UITableView, willDisplay cell:UITableViewCell, forRowAt indexPath: IndexPath){
        let lastitem = listCoinDataSrc.count-1
        if(lastitem2 ==  lastitem){
            print("khong load nua")
        }else{
            if(indexPath.section == lastitem){
                print("tiep tuc load")
                self.page+=1
                lastitem2 =  lastitem
                self.getDataFromAPIToLoadList(page:String(self.page))
            }
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return listCoinDataSrc.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Allcoin.identifier, for: indexPath) as! Allcoin
        let coin = listCoinDataSrc[indexPath.section]
        cell.coinname.text=coin.id?.uppercased()
        cell.smcoinname.text=coin.symbol?.uppercased()
        cell.priceone.text = String(coin.current_price ?? 0.0)
        cell.btnfavou.isEnabled = true
        cell.isUserInteractionEnabled = true
        if coin.favourite == true {
            styleFavouriteBtn(dataList:coin,cell: cell, isFavourite: true)
        }else{
            styleFavouriteBtn(dataList:coin,cell: cell, isFavourite: false)
        }
       
        cell.btnfavou.isEnabled = true
        cell.delegate = self
        if let imgcache = coin.loadimgage
        {
            cell.coinimg.image = imgcache
            return cell
        }
        if let nonimage = coin.image {
            
            DispatchQueue.global().async {
            var data :Data
            data = CommonServices.loadImgFromUrl(url: nonimage)
                DispatchQueue.main.async {
                    cell.coinimg.image = UIImage(data: data)
                    coin.loadimgage = cell.coinimg.image
                }
            }
           
        }
        return cell
    }
    func styleFavouriteBtn(dataList:Listcoin,cell: Allcoin,isFavourite :  Bool){
        if isFavourite
            {
            cell.childkey = dataList.childkey!
            cell.favouriteflag = 1
            cell.btnfavou.setImage(UIImage(systemName: "heart.fill"), for: .normal)
        }else{
            cell.favouriteflag = 0
            cell.childkey = ""
            cell.btnfavou.setImage(UIImage(systemName: "heart"), for: .normal)
        }
    }

}
