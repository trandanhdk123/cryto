//
//  DashboardViewController.swift
//  cryto
//
//  Created by VN Grand M on 3/21/22.
//

import UIKit
import Firebase


@objc protocol delegatetest {
    func passdata(data : String)
    @objc optional func checkdatasrc(data : String) ->String
}
class DashboardViewController: UIViewController	 {
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var tabbar: UITabBarItem!
    var page = 1
    var listCoinDataSrc=[Listcoin]()
    var beforeFilterData = [Listcoin]()
    var datatable : [String:String] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        print("we have user default \(String(describing: UserDefaults.standard.dictionaryWithValues(forKeys: ["id"])))")
        if(FirebaseApp.app() == nil){
            FirebaseApp.configure()
        }
        tableview.register(Allcoin.nib(), forCellReuseIdentifier: Allcoin.identifier)
        tableview.delegate=self
        tableview.dataSource=self
        getDataFromAPIToLoadList(page:String(self.page))
        unFavouriteCoin()
        dashboardNav()
    }
    let search =  UISearchController()
    var delegate :delegatetest?
    var lastitem2 : Int = 0
    func getDataFromAPIToLoadList(page  : String){
        let group = DispatchGroup()
        group.enter()
        DashboardNetworking.getCoins(listcoins: self.listCoinDataSrc,page: String(self.page)){ (coins) in
            self.listCoinDataSrc = coins
            self.tableview.reloadData()
            self.present(CommonServices.loaddingScreen(), animated: true, completion: nil)
            group.leave()
        }
        group.notify(queue: .global()) {
            DashboardNetworking.loadFavouriteCoinForDashboard(listCoinDataSrc: self.listCoinDataSrc){(coinsListWithFavouriteFlag) in
                self.listCoinDataSrc = coinsListWithFavouriteFlag
                self.tableview.reloadData()
              
            }
            DispatchQueue.main.sync {
                self.dismiss(animated: false, completion: nil)
            }
      
        }
    }
    func unFavouriteCoin(){
        let group = DispatchGroup()
        group.enter()
        DashboardNetworking.updateListCoinAfterUnFavourite(listCoinDataSrc:self.listCoinDataSrc){(listCoinAfterRemove) in
            self.listCoinDataSrc.first{$0.id == listCoinAfterRemove}?.favourite = false
            self.listCoinDataSrc.first{$0.id == listCoinAfterRemove}?.childkey = ""
            self.tableview.reloadData()
            self.present(CommonServices.loaddingScreen(), animated: true, completion: nil)
          
        }
        group.leave()
        group.notify(queue: .main) {
            self.dismiss(animated: false, completion: nil)
        }
    }

    func favourite(coinid :String, type : String, childkey : String) {
        let group = DispatchGroup()
        group.enter()
        var ref: DatabaseReference!
        let currentus = CommonServices.authfirebase()
        ref = Database.database().reference()
        
        if(type == "favourite"){
            let group = DispatchGroup()
            group.enter()
            ref.child("favourite").child(currentus).childByAutoId().setValue(coinid.lowercased())
            self.present(CommonServices.loaddingScreen(), animated: true, completion: nil)
            group.leave()
            group.notify(queue: .main) {
                self.dismiss(animated: false, completion: nil)
            }
        }else{
            let group = DispatchGroup()
            group.enter()
            ref.child("favourite").child(currentus).child(childkey).removeValue()
            self.present(CommonServices.loaddingScreen(), animated: true, completion: nil)
            group.leave()
            group.notify(queue: .main) {
                self.dismiss(animated: false, completion: nil)
            }
        }
        
        print("I like \(coinid)")
    }
   

}


