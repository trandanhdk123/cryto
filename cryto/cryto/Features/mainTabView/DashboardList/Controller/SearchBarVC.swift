//
//  searchBarVc.swift
//  cryto
//
//  Created by VN Grand M on 4/20/22.
//

import UIKit
extension DashboardViewController : UISearchResultsUpdating, UISearchBarDelegate {
    func dashboardNav (){
        navigationController?.setNavigationBarHidden(false, animated: true)
        let leftButton = UIBarButtonItem(title: "Info", style: .plain, target: navigationController, action: nil)
       navigationItem.leftBarButtonItem = leftButton
        let rightButton = UIBarButtonItem(title: "Buy", style: .plain, target: navigationController, action: nil)
        navigationItem.rightBarButtonItem = rightButton
        navigationItem.title = "Stock"
        search.searchResultsUpdater = self
        search.dimsBackgroundDuringPresentation = false
        search.searchBar.sizeToFit()
        definesPresentationContext = true
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.searchController = search
        navigationItem.hidesSearchBarWhenScrolling = true
        search.searchBar.delegate = self
     
            
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        listCoinDataSrc = beforeFilterData
        tableview.reloadData()
    }
    func updateSearchResults(for searchController: UISearchController) {
        if(beforeFilterData.isEmpty) {
            beforeFilterData = listCoinDataSrc
        }
     
        if let searchText = search.searchBar.text {
            listCoinDataSrc = searchText.isEmpty ? listCoinDataSrc : listCoinDataSrc.filter{
                $0.id!.contains(searchText) }
         
            
              tableview.reloadData()
          }
    }
}
