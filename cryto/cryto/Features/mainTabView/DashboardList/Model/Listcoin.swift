//
//  Listcoin.swift
//  cryto
//
//  Created by VN Grand M on 3/21/22.
//

import Foundation
import UIKit
import  ObjectMapper

class Listcoin: Mappable {
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        self.symbol <- map["symbol"]
        self.image <- map["image"]
        self.id <- map["id"]
        self.current_price <- map["current_price"]

    }
    
  public  var id:String?;
    public var symbol : String?;
    public var image : String?;
    public var current_price : Double?
    public var loadimgage :UIImage?
    public var favourite : Bool?
    public var childkey : String?

}

