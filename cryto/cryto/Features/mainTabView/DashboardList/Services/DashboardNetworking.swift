//
//  dashboardNetworking.swift
//  cryto
//
//  Created by VN Grand M on 4/20/22.
//

import Foundation
import ObjectMapper
class DashboardNetworking {
    private init(){}
    static func getCoins(listcoins: [Listcoin],page: String, completion: @escaping([Listcoin])->Void){
        var listcoins = listcoins
        NetworkServices.shared.getAllCoinFromAPI(page: page){(coins) in
            if let coins = coins as? [[String:AnyObject]] {
            for coin in coins {
                if let coin = Listcoin(JSON: coin)
                  {
                    listcoins.append(coin)
                    
                    }
            }
                completion(listcoins)
                
          }
        }
        
    }
    static func loadFavouriteCoinForDashboard(listCoinDataSrc:[Listcoin],completion:@escaping([Listcoin])->Void){
        NetworkServices.shared.getFavouriteCoinFromDB(){(favouriteCoinGetFromDB) in
            for (key,value) in favouriteCoinGetFromDB {
               listCoinDataSrc.first{$0.id == value}?.favourite = true
                listCoinDataSrc.first{$0.id == value}?.childkey = key
            }
            completion(listCoinDataSrc)
            
        }
    }
    static func updateListCoinAfterUnFavourite(listCoinDataSrc:[Listcoin],completion:@escaping(String)->Void){
        NetworkServices.shared.handleRemoveRecordDBEvent(){(favouriteCoinRemoved) in
           
            completion(favouriteCoinRemoved.value as! String)
            
        }
        
    }
}
