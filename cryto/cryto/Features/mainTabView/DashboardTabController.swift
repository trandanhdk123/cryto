//
//  DashboardTabController.swift
//  cryto
//
//  Created by VN Grand M on 3/21/22.
//

import UIKit

class DashboardTabController: UITabBarController {

    @IBOutlet weak var tabbar: UITabBar?
    override func viewDidLoad() {
        super.viewDidLoad()
        if let firstTab = self.tabBar.items?[0] {
            firstTab.title = "Home"
            firstTab.image = UIImage(named: "dashboardTabIconSelected")
            firstTab.selectedImage = UIImage(systemName: "dashboardTabIconSelected")
                }
        if let secondTab = self.tabBar.items?[1] {
            secondTab.title = "Favourite"
            secondTab.image = UIImage(named: "favouriteTabIconSelected")
            secondTab.selectedImage = UIImage(named: "favouriteTabIconSelected")
                }
        if let thirdTab = self.tabBar.items?[2] {
            thirdTab.title = "Author"
            thirdTab.image = UIImage(named: "authorTabIconSelected")
            thirdTab.selectedImage = UIImage(named: "authorTabIconSelected")
                }
        
      
    }
}
