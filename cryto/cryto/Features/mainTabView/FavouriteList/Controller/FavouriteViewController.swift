//
//  FavouriteViewController.swift
//  cryto
//
//  Created by VN Grand M on 3/29/22.
//

import UIKit
class FavouriteViewController: UIViewController {
    @IBOutlet weak var tablefavourite: UITableView!
    @IBOutlet weak var labelfv: UILabel!
    @objc func editmodefunc(_ sender : UIButton){

        if tablefavourite.isEditing{
            tablefavourite.setEditing(false, animated: true)
            
        }else{
            tablefavourite.setEditing(true, animated: true)
        }
    }
    var favouriteDataSrc=[CoinDetail]()
    var snap : [String:String] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        tablefavourite.register(Allcoin.nib(), forCellReuseIdentifier: Allcoin.identifier)
        tablefavourite.delegate=self
        tablefavourite.dataSource=self
        favouriteNav()
        loadFavouriteCoinForList()
    }
    override func viewWillAppear(_ animated: Bool) {
        loadFavouriteCoinForList()
    }
    func loadFavouriteCoinForList(){
        let group = DispatchGroup()
        FavouriteListNetworking.getAllCoinFavouriteID(){(allFavouriteCoinID) in
            group.enter()
            self.snap = allFavouriteCoinID
            self.present(CommonServices.loaddingScreen(), animated: true, completion: nil)
            group.leave()
        }
        group.notify(queue: .main) {
            self.favouriteDataSrc = []
            for (key,value) in self.snap{
                FavouriteListNetworking.loadCoinFromAPIWithCoinID(id: value,listcoin: self.favouriteDataSrc, childkey:key) {(coinDetail) in
                    self.favouriteDataSrc.append(contentsOf: coinDetail)
                    self.tablefavourite.reloadData()
                    
                }
            }
            self.dismiss(animated: false, completion: nil)
        }
    }
    func favouriteNav(){
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.title = "Favourite"
        let leftButton = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(editmodefunc(_:)))
       navigationItem.leftBarButtonItem = leftButton
        let rightButton = UIBarButtonItem(title: "", style: .plain, target: navigationController, action: nil)
        navigationItem.rightBarButtonItem = rightButton
        navigationItem.searchController = nil
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    var delegate :delegatetest?
}
