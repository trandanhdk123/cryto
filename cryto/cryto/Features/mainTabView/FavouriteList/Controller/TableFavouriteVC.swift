//
//  tableFavouriteVC.swift
//  cryto
//
//  Created by VN Grand M on 4/20/22.
//

import Foundation
import UIKit
import Firebase
extension FavouriteViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return favouriteDataSrc.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let dashview = storyboard.instantiateViewController(identifier: "detail") as! CoinDetailVC
        self.delegate = dashview
        self.delegate?.passdata(data: favouriteDataSrc[indexPath.row].id!)
        dashview.modalPresentationStyle = .fullScreen
        navigationController?.pushViewController(dashview, animated: true)
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            var ref: DatabaseReference!
            let currentus = CommonServices.authfirebase()
            ref = Database.database().reference()
            ref.child("favourite").child(currentus).child(favouriteDataSrc[indexPath.row].childkey!).removeValue()
            print("A")
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Allcoin.identifier, for: indexPath) as! Allcoin
        let coin = favouriteDataSrc[indexPath.row]
        cell.coinname.text=coin.symbol?.uppercased()
        cell.smcoinname.text=coin.id?.uppercased()
        let current_price = coin.market_data!["current_price"] as! [String:AnyObject]
        cell.priceone.text =  current_price["usd"] as? String
        let iumgurl = coin.image!["thumb"] as! String
        cell.btnfavou.setImage(UIImage(systemName: "heart.fill"), for: UIControl.State.normal)
        if let imgcache = coin.loadimage
        {
            cell.coinimg.image = imgcache
            return cell
        }
        DispatchQueue.global().async {
        var data :Data
        data = CommonServices.loadImgFromUrl(url: iumgurl)
            DispatchQueue.main.async {
                cell.coinimg.image = UIImage(data: data)
                coin.loadimage = cell.coinimg.image
            }
        }
        cell.btnfavou.setImage(UIImage(systemName: "heart.fill"), for: UIControl.State.normal)
        return cell
    }
}
