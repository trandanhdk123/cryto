//
//  coinDetail.swift
//  cryto
//
//  Created by VN Grand M on 4/20/22.
//

import Foundation
import UIKit
import ObjectMapper
class CoinDetail :Mappable {
    required init?(map: Map){
        
    }
    func mapping(map: Map) {
        self.symbol <- map["symbol"]
        self.image <- map["image"]
        self.id <- map["id"]
        self.market_data <- map["market_data"]
        
    }
    public  var id:String?;
    public var symbol : String?;
    public var image : [String:AnyObject]?;
    public var market_data : [String:AnyObject]?
    public var loadimage :UIImage?
    public var childkey : String?
    public var price_change_24h : String?

}
