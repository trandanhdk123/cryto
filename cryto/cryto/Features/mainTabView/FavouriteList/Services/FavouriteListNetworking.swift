//
//  favouriteListNetWorking.swift
//  cryto
//
//  Created by VN Grand M on 4/20/22.
//

import Foundation
class FavouriteListNetworking{
    static func getAllCoinFavouriteID(completion:@escaping([String:String])->Void){
        NetworkServices.shared.getFavouriteCoinFromDB(){(favouriteCoins) in
            completion(favouriteCoins)
        }
    }
    static func loadCoinFromAPIWithCoinID(id:String,listcoin:[CoinDetail],childkey : String,completion : @escaping([CoinDetail])->Void){
        var listcoin = listcoin
        NetworkServices.shared.getDataOfCoinByCoinID(id: id) {(coinData) in
            coinData.childkey = childkey
            listcoin.append(coinData)
            completion(listcoin)
        }
    }
}
