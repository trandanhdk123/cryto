//
//  EditProfileTableViewController.swift
//  cryto
//
//  Created by VN Grand M on 4/15/22.
//

import UIKit
import  Firebase
class EditProfileTableViewController: UITableViewController, UITextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        setBackBtnIconNav(imgName: "signupBackIcon")
        setRightBtnIconNav(title: "Save", action: #selector(saveEditProfileData(_:)))
        rightNavBtnColor(hexString: "#FFFFFF")
        loadProfileDataFromDB()
    }
    var userData = [String:String]()
    func loadProfileDataFromDB(){
        UserProfileNetworking.loadUserDataToView(){(userData)in
                    self.userData = userData
                    self.tableView.reloadData()
                }
       
    }
    @objc func saveEditProfileData(_ sender :  UINavigationItem){
                
        UserProfileNetworking.saveUserDataToDB(userData: userData){_ in
            self.navigationController?.popViewController(animated: true)
        }
                
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if let textfieldID = textField.accessibilityIdentifier {
            userData[textfieldID] = textField.text
        }
        print(userData)
        
    }
    @objc func dateChange(picker :  UIDatePicker){
        if let pickerID = picker.accessibilityIdentifier {
            let timeFormatter = DateFormatter()
           dateTimeFormat(date: timeFormatter)
            userData[pickerID] = timeFormatter.string(from: picker.date)
        }
        print(picker.date)
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 4
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 90
        case 1,2,3 :
            return 65
        default: return 61
            
        }
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0   : return 16
        case 1   : return 17
        case 2,3   : return 40
        default  : return 61
            
        }
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           let headerView = UIView()
            headerView.backgroundColor = UIColor.init(hexString: "#FFFFFF")
           return headerView
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        print(userData)
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "avatar", for: indexPath)
            cell.selectionStyle = .none
            return cell
        case 1 :
            let cell = tableView.dequeueReusableCell(withIdentifier: "birthday", for: indexPath) as! CellMultiCellProfile
            cell.birthday.isUserInteractionEnabled = true
            cell.birthday.accessibilityIdentifier  = "birthday"
            let timeFormatter = DateFormatter()
           dateTimeFormat(date: timeFormatter)
            if let dob = userData["birthday"]{
                cell.birthday.date = timeFormatter.date(from: dob)!
            }
            cell.birthday.addTarget(self, action: #selector(dateChange(picker:)), for: .valueChanged)
            cell.sex.isUserInteractionEnabled = true
            cell.sex.accessibilityIdentifier  = "sex"
            cell.sex.text  = userData["sex"]
            cell.sex.delegate =  self
            cell.selectionStyle = .none
            return cell
        case 2 :
            let cell = tableView.dequeueReusableCell(withIdentifier: "job", for: indexPath) as! CellMultiCellProfile
            cell.selectionStyle = .none
            cell.job.isUserInteractionEnabled = true
            cell.job.accessibilityIdentifier  = "job"
            cell.job.text = userData["job"]
            cell.job.delegate = self
            return cell
        case 3 :
            let cell = tableView.dequeueReusableCell(withIdentifier: "heightAndWeight", for: indexPath) as! CellMultiCellProfile
            cell.selectionStyle = .none
            cell.height.isUserInteractionEnabled = true
            cell.height.accessibilityIdentifier  = "height"
            cell.height.delegate = self
            cell.height.text = userData["height"]
            cell.height.keyboardType = .emailAddress
            cell.weight.isUserInteractionEnabled = true
            cell.weight.accessibilityIdentifier  = "weight"
            cell.weight.delegate =  self
            cell.weight.text = userData["weight"]
            cell.weight.keyboardType = .emailAddress
            return cell
        default: return CellMultiCellProfile()
            
        }
       

}
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
