//
//  ProfileMultiCellTableViewController.swift
//  cryto
//
//  Created by VN Grand M on 4/19/22.
//

import UIKit
class ProfileMultiCellTableViewController: UITableViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        loadProfileDataFromDB()
        hideBackBtn()
        setNavColor(hexString: "#0062CF")
        setRightBtnIconNav(title: "Edit", action: #selector(moveToEditProfile(_:)))
        rightNavBtnColor(hexString: "#FFFFFF")
    }
    override func viewWillAppear(_ animated: Bool) {
        loadProfileDataFromDB()
    }
    @objc func moveToEditProfile(_ sender :  UINavigationItem){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let editProfile = storyboard.instantiateViewController(identifier: "editProfileScreen") as! EditProfileTableViewController
        editProfile.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(editProfile, animated: true)
    }
    var userData = [String:String]()
    func loadProfileDataFromDB(){
        UserProfileNetworking.loadUserDataToView(){ (userData) in
            self.userData = userData
            self.tableView.reloadData()
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 7
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 90
        case 1,2 :
            
            return 20
     
        case 3,4,5 :
           
            return 65
        case 6 :
            return 85
        default: return 61
            
        }
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0   : return 16
        case 1   : return 8
        case 2   : return 4
        case 3   : return 33
        case 4,5 : return 45
        default  : return 61
            
        }
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           let headerView = UIView()
            headerView.backgroundColor = UIColor.init(hexString: "#FFFFFF")
           return headerView
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(userData)
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "avatar", for: indexPath)
            cell.selectionStyle = .none
            return cell
        case 1,2 :
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameOrEmail", for: indexPath) as! CellMultiCellProfile
            if indexPath.section == 1 {
                cell.emailOrName.text = userData["name"]
                cell.emailOrName.text?.append(" ")
                cell.emailOrName.text?.append(userData["Firstname"]!)
            }else{
                cell.emailOrName.text = userData["email"]
            }
            cell.emailOrName.textColor =  UIColor.init(hexString: "1D1D26")
          
            cell.selectionStyle = .none
            return cell
        case 3 :
            let cell = tableView.dequeueReusableCell(withIdentifier: "birthday", for: indexPath) as! CellMultiCellProfile
            let timeFormatter = DateFormatter()
           dateTimeFormat(date: timeFormatter)
            if let date =  userData["birthday"] {
                cell.birthday.date = timeFormatter.date(from: date)!
            }
            cell.sex.text = userData["sex"]
            cell.selectionStyle = .none
            return cell
        case 4 :
            let cell = tableView.dequeueReusableCell(withIdentifier: "job", for: indexPath) as! CellMultiCellProfile
            cell.job.text = userData["job"]
            cell.selectionStyle = .none
            return cell
        case 5 :
            let cell = tableView.dequeueReusableCell(withIdentifier: "heightAndWeight", for: indexPath) as! CellMultiCellProfile
            cell.height.text = userData["height"]
            cell.weight.text = userData["weight"]
            cell.selectionStyle = .none
            return cell
        case 6 :
            let cell = tableView.dequeueReusableCell(withIdentifier: "logoutBtnCell", for: indexPath) as! CellMultiCellProfile
            self.shadowBtn(btn: cell.logoutButton)
            self.borderRadius(btn: cell.logoutButton)
            self.btnBackgroundColor(btn: cell.logoutButton, color: "#0062CF")
            cell.selectionStyle = .none
            return cell
        default: return CellMultiCellProfile()
            
        }
       
    }
  

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
