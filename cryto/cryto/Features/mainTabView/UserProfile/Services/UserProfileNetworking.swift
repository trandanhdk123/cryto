//
//  userProfileNetworking.swift
//  cryto
//
//  Created by VN Grand M on 4/21/22.
//

import Foundation
import UIKit
class UserProfileNetworking{
    static func loadUserDataToView(completion: @escaping([String:String])->Void){
        NetworkServices.shared.getUserDataFromDB(){(userDataFromDB) in
            let userDataFromDB = userDataFromDB.value as! [String:String]
            completion(userDataFromDB)
            
        }
    }
    static func saveUserDataToDB(userData:[String:String], completion:@escaping(Bool)->Void){
        NetworkServices.shared.saveUserDataToDB(userData: userData){
            completion(true)
        }
    }
}
