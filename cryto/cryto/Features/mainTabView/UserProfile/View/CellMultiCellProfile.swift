//
//  cellMultiCellProfile.swift
//  cryto
//
//  Created by VN Grand M on 4/21/22.
//

import Foundation
import UIKit
class CellMultiCellProfile:  UITableViewCell{
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var emailOrName: UILabel!
    @IBOutlet weak var birthday: UIDatePicker!
    @IBOutlet weak var lblBirthday: UILabel!
    @IBOutlet weak var sex: UITextField!
    @IBOutlet weak var lblSex: UILabel!
    @IBOutlet weak var job: UITextField!
    @IBOutlet weak var height: UITextField!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var weight: UITextField!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblJob: UILabel!
    @IBOutlet weak var logoutButton : UIButton!
    @IBAction func logout (_ sender:UIButton){
        UserDefaults.standard.set(false, forKey: "Loggedin")
       let storyboard = UIStoryboard(name: "Main", bundle: nil)
       let root = storyboard.instantiateViewController(identifier: "auth")
       root.modalPresentationStyle = .fullScreen
       UIApplication.shared.windows.first?.rootViewController = root
       UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    override func awakeFromNib() {
      
    }
   
}
