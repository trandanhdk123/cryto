//
//  MoyaTest.swift
//  cryto
//
//  Created by VN Grand M on 3/23/22.
//

import Foundation
import Moya
enum MoyaTest {
    case listdashboard(vs_currency : String, order :String ,per_page :String,page :String,sparkline : Bool)
    case chartdata(id :String, vs_currency : String , days : String)
    case listfavourite(id: String)
}
extension MoyaTest : TargetType {
    
    
    var baseURL: URL {
        URL(string: "https://api.coingecko.com/api/v3")!
    }
    
    var path: String {
        switch self {
        case .listdashboard:
            return "/coins/markets"
        case .chartdata(let id,_,_) :
            return "coins/\(id)/market_chart"
        case .listfavourite(id: let id):
            return "/coins/\(id)"
        }
        
    }
    
    var method: Moya.Method {
        switch self {
        case .listdashboard:
            return .get
        case .chartdata:
            return .get
        case .listfavourite:
           return .get
        }
        
    }
    
    var task: Task {
        switch self {
        case let .listdashboard (vs_currency,order,per_page,page,sparkline):
            return .requestParameters(parameters: ["vs_currency":vs_currency,"order":order,"per_page":per_page,"page":page,"sparkline":sparkline], encoding: URLEncoding.default)
            
        case let .chartdata(_,vs_currency,days):
            return .requestParameters(parameters: ["vs_currency":vs_currency,"days":days], encoding: URLEncoding.default)
        case .listfavourite(_):
            return .requestPlain
        }
       
    }
    var sampleData: Data{
        return Data()
    }
    var headers: [String : String]? {
        return ["Content-type" : "application/json"]
    }
    
    
}
