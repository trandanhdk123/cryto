//
//  networkSevices.swift
//  cryto
//
//  Created by VN Grand M on 4/20/22.
//

import Foundation
import Firebase
import Moya
import ObjectMapper
class NetworkServices {
    static var shared = NetworkServices()
    func getAllCoinFromAPI(page: String,  completion: @escaping (Any) -> Void)  {
        let provider = MoyaProvider<MoyaTest>()
        provider.request(.listdashboard(vs_currency : "usd", order :"market_cap_desc" ,per_page :"100",page : page,sparkline : false)){result in
                switch result {
                    case let .success(moyaResponse):
                        let data = moyaResponse.data
                        var jsonArray: Array<Any>!
                        do {
                            jsonArray = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? Array
                            completion(jsonArray)
                        } catch {
                          print(error)
                        }
                    case let .failure(error):
                        print(error)
                        break
            }
        }
    }
    func getFavouriteCoinFromDB(completion: @escaping ([String:String])->Void){
        var ref: DatabaseReference!
        let currentus = CommonServices.authfirebase()
        ref = Database.database().reference()
        ref.child("favourite").child(currentus).observe(.value, with: { snapshot in
            if(snapshot.value! as! NSObject == NSNull()){
            }else{
                completion(snapshot.value as! [String:String])
            }
        })
    }
    func handleRemoveRecordDBEvent(completion: @escaping (DataSnapshot)->Void){
        var ref: DatabaseReference!
        let currentus = CommonServices.authfirebase()
        ref = Database.database().reference()
        ref.child("favourite").child(currentus).observe(.childRemoved, with: { snapshot in
            if(snapshot.value! as! NSObject == NSNull()){
            }else{
                completion(snapshot)
            }
        })
    }
    func getDataOfCoinByCoinID(id:String, completion: @escaping(CoinDetail)->Void){
        let provider = MoyaProvider<MoyaTest>()
        provider.request(.listfavourite(id: id)){result in
            switch result {
                case let .success(moyaResponse):
                    let data = moyaResponse.data
                    var jsonArray: [String:AnyObject]!
                    do {
                        jsonArray = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
                    } catch {
                      print(error)
                    }
                    if let jsonArray = jsonArray {
                        if let coin = CoinDetail(JSON: jsonArray)
                            {
                            completion(coin)
                            
                            }
                        }
                case let .failure(error):
                    print(error)
                    break
            }
        }
    }
    func getUserDataFromDB(completion: @escaping(DataSnapshot)->Void){
        CommonServices.dbFireBaseInit()
        var ref: DatabaseReference!
        let currentus = CommonServices.authfirebase()
        ref = Database.database().reference()
        ref.child("users").child(currentus).getData(completion:  { error, snapshot in
            completion(snapshot)
        })
    }
    func saveUserDataToDB(userData:[String:String], completion: @escaping()->Void){
        var ref: DatabaseReference!
        ref = Database.database().reference()
        let currentus = CommonServices.authfirebase()
        ref.child("users").child(currentus).updateChildValues(userData)
    }
    func firebaseAuthorWithEmailPassword(email:String, password:String, completion: @escaping(Bool)->Void){
        Auth.auth().signIn(withEmail: email, password: password) { [weak self] authResult, error in
            guard self != nil else { return completion(false) }
            if(error == nil ){
                completion(true)
            }else{
                completion(false)
            }
            }
    }
    func checkIfEmailIsVerified(completion:@escaping(Bool)->Void){
        if  Auth.auth().currentUser!.isEmailVerified {
            completion(true)
        }else{
            completion(false)
        }
    }
    func firebaseCreateUserWithEmailPassword(email:String, password:String,completion:@escaping(Bool)->Void){
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            if((error == nil)){
                completion(true)
            }else{
                completion(false)
            }
        }
    }
    func firebaseIsSendEmailVerified(completion: @escaping(Bool)->Void){
        Auth.auth().currentUser?.sendEmailVerification { (error) in
            if(error == nil){
                completion(true)
            }else{
                completion(false)
            }
        }
    }
    func getChartDataFromAPI(time:String, id:String,completion : @escaping(CoinChartData)->Void) {

            let provider = MoyaProvider<MoyaTest>()
            provider.request(.chartdata(id: id, vs_currency: "usd", days: time)){ result in
                switch result {
                case let .success(moyaresponse):
                    do{
                        let myJSON = try JSONDecoder().decode(CoinChartData.self, from: moyaresponse.data)
                        completion(myJSON)
                    }catch {
                        print(error)
                    }
                case let .failure(error):
                    print(error)
                }

            }
    }


}
