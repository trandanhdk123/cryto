//
//  tablcellExtension.swift
//  cryto
//
//  Created by VN Grand M on 4/21/22.
//

import Foundation
import UIKit
extension UITableViewCell{
    func shadowBtn(btn : UIButton)  {
        btn.layer.shadowColor = UIColor.black.cgColor
        btn.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        btn.layer.shadowRadius = 4
        btn.layer.shadowOpacity = 1
    }
    func borderRadius(btn : UIButton)  {
        btn.layer.cornerRadius =  5
    }
    func btnBackgroundColor(btn: UIButton, color :  String)  {
        btn.backgroundColor = UIColor.init(hexString: color)
    }
}
