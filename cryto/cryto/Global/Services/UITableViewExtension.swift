//
//  uitableviewExtension.swift
//  cryto
//
//  Created by VN Grand M on 4/21/22.
//

import Foundation
import UIKit

extension UITableViewController {

    func dateTimeFormat(date  : DateFormatter){
   
        date.dateFormat = "yyyy-MM-dd"
        
    }
    
    func isInVisibleNavBar (isVisible: Bool, animated : Bool ){
       navigationController?.setNavigationBarHidden(isVisible, animated: animated)
    }
    func setBackBtnIconNav(imgName : String){
        var backIcon = UIImage(named: imgName)
        self.navigationController?.navigationBar.backIndicatorImage = backIcon
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backIcon
    }
    func setRightBtnIconNav(title: String , action  :Selector){
        let rightButton = UIBarButtonItem(title: title, style: .plain, target: self, action: action)
        navigationItem.rightBarButtonItem = rightButton
      
    }
    func rightNavBtnColor(hexString: String){
        navigationItem.rightBarButtonItem?.tintColor =  UIColor.init(hexString: hexString)
        navigationItem.backBarButtonItem?.tintColor =  UIColor.init(hexString: hexString)
    }
    func hideBackBtn(){
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        navigationItem.backBarButtonItem?.tintColor =  UIColor.init(hexString: "#FFFFFF")
    }
    func setNavColor(hexString : String){
        self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: hexString)
    }
    func shadowBtn(btn : UIButton)  {
        btn.layer.shadowColor = UIColor.black.cgColor
        btn.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        btn.layer.shadowRadius = 4
        btn.layer.shadowOpacity = 1
    }
    func borderRadius(btn : UIButton)  {
        btn.layer.cornerRadius =  5
    }
    func btnBackgroundColor(btn: UIButton, color :  String)  {
        btn.backgroundColor = UIColor.init(hexString: color)
    }
    func changeRootToTabView(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let root = storyboard.instantiateViewController(identifier: "dash")
        root.modalPresentationStyle = .fullScreen
        UIApplication.shared.windows.first?.rootViewController = root
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}
